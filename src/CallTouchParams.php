<?php

namespace Procontext\CallTouch;

use Procontext\CallTouch\Exception\ValidationException;
use \ReflectionClass;
use \ReflectionProperty;

class CallTouchParams
{
    /**
     * Ключ формы. (Обязательное поле, произвольная строка, может отличаться для разных форм)
     *
     * @var string
     */
    public $routeKey;

    /**
     * Номер телефона клиента. (Обязательное поле)
     *
     * @var string
     */
    public $phone;

    /**
     * Id сессии. (Необязательное поле, получается с помощью window.call_value)
     *
     * @var integer
     */
    public $sessionId = 0;

    /**
     * Источник перехода по рекламе
     *
     * @var string
     */
    public $utmSource;

    /**
     * Тип трафика
     *
     * @var string
     */
    public $utmMedium;

    /**
     * Обозначение рекламной кампании
     *
     * @var string
     */
    public $utmCampaign;

    /**
     * Содержание кампании
     *
     * @var string
     */
    public $utmContent;

    /**
     * Условие поиска кампании
     *
     * @var string
     */
    public $utmTerm;

    protected $properties;
    protected $errors;

    public function __construct(array $formData)
    {
        if(!$this->routeKey = env('CT_ROUTE_KEY', null)) {
            $this->errors[] = 'Ключ виджета не задан в конфигурации';
        }

        $this->formatted($formData);
        $formData = $this->preload($formData);
        $reflection = new ReflectionClass(get_called_class());
        $this->properties = array_column($reflection->getProperties(ReflectionProperty::IS_PUBLIC), 'name');
        foreach ($this->properties as $property) {
            if (isset($formData[$property])) {
                if ($property == 'phone' && ((string) $formData[$property])[0] == '8') {
                    $formData[$property][0] = '7';
                }
                $this->{$property} = $formData[$property];
            } elseif(!isset($this->{$property})) {
                $this->errors[] = "Параметр " . $property . " не задан";
            }
        }

        if($this->errors) {
            throw new ValidationException($this->errors);
        }
    }

    protected function preload(array $formData): array
    {
        $formData['sessionId'] = array_get($formData, 'session_id', 0);
        return $formData;
    }

    public function export(): array
    {
        $formData = [];
        foreach ($this->properties as $property) {
            if($property == 'sessionId' && $this->{$property} == 0) {
                continue;
            }
            $formData[$property] = $this->{$property};
        }
        return $formData;
    }

    public function errors(): array
    {
        return $this->errors;
    }

    private function formatted(array &$formData): void
    {
        foreach ($formData as $key => $item) {
            switch ($key) {
                case 'utm_source':
                    $formData['utmSource'] = $item;
                    unset ($formData['utm_source']);
                    break;
                case 'utm_medium':
                    $formData['utmMedium'] = $item;
                    unset ($formData['utm_medium']);
                    break;
                case 'utm_campaign':
                    $formData['utmCampaign'] = $item;
                    unset ($formData['utm_campaign']);
                    break;
                case 'utm_content':
                    $formData['utmContent'] = $item;
                    unset ($formData['utm_content']);
                    break;
                case 'utm_term':
                    $formData['utmTerm'] = $item;
                    unset ($formData['utm_term']);
                    break;
                default:
                    break;
            }
        }
    }
}
