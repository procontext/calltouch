<?php

namespace Procontext\CallTouch\Exception;

use \Exception;
use Throwable;

class CallTouchDisabledException extends Exception
{
    public function __construct($message = "CallTouch API отключен", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}